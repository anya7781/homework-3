//
//  DegreeOperation.m
//  Calculator
//
//  Created by Admin on 03.11.16.
//  Copyright © 2016 Admin. All rights reserved.
//

#import "DegreeOperation.h"

@implementation DegreeOperation

- (double) performWithSecondArgument: (double) SecondArgument  {
    return pow (self.firstArgument, SecondArgument);
}


@end
