//
//  CalculatorModel.m
//  Calculator
//
//  Created by Admin on 03.11.16.
//  Copyright © 2016 Admin. All rights reserved.
//

#import "CalculatorModel.h"
#import "SumOperation.h"
#import "SignOperation.h"
#import "EqualOperation.h"
#import "SubOperation.h"
#import "CleanOperation.h"
#import "PercentOperation.h"
#import "MulOperation.h"
#import "DelOperation.h"
#import "DegreeOperation.h"
#import "SinOperation.h"
#import "CosOperation.h"
#import "TanOperation.h"
#import "SqrtOperation.h"

@implementation CalculatorModel

- (instancetype) init {
    self = [super init];
    if (self){
        _operations = @{
                        @"+": [[SumOperation alloc] init],
                        @"+-": [[SignOperation alloc] init],
                        @"=": [[EqualOperation alloc] init],
                        @"-": [[SubOperation alloc] init],
                        @"%": [[PercentOperation alloc] init],
                        @"*": [[MulOperation alloc] init],
                        @"/": [[DelOperation alloc] init],
                        @"AC": [[CleanOperation alloc] init],
                        @"cos": [[CosOperation alloc] init],
                        @"sin": [[SinOperation alloc] init],
                        @"tan": [[TanOperation alloc] init],
                        @"^": [[DegreeOperation alloc] init],
                        @"√": [[SqrtOperation alloc] init],
                        };
    }
    return self;
}

-(id) operationForOperationString: (NSString *) operationString {
    id operation = [self.operations objectForKey:operationString];
    if (!operation){
        operation = [[UnaryOperation alloc] init]; //if incorrect operation (we haven't into the Dictionary)
    }
    return operation;
}

-(double) performOperation: (NSString*) operationString withValue:(double)value {
    
    id operation = [self operationForOperationString:operationString]; //type of the operation
    
    if ([operation isKindOfClass:[EqualOperation class]]){
        if (self.curentBinaryOperation){
            value = [self.curentBinaryOperation performWithSecondArgument:value]; //action of the binary operation (first_el, operation - remember, second_el - value)
        }
    }
    
    if ([operation isKindOfClass:[UnaryOperation class]]){
        return [operation perform: value];
    }
    
    if ([operation isKindOfClass:[BinaryOperation class]]){
        self.curentBinaryOperation = operation; //remember operation
        self.curentBinaryOperation.firstArgument = value; //first point - remember string before sign/ second point -  result
        return value; //string before sign
    }
    return DBL_MAX;
    
}

@end
