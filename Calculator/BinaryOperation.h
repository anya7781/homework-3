//
//  BinaryOperation.h
//  Calculator
//
//  Created by Admin on 02.11.16.
//  Copyright © 2016 Admin. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface BinaryOperation : NSObject

@property (nonatomic, assign) double firstArgument;

- (double) performWithSecondArgument: (double) secondArgument;

@end
