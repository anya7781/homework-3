//
//  SumOperation.m
//  Calculator
//
//  Created by Admin on 02.11.16.
//  Copyright © 2016 Admin. All rights reserved.
//

#import "SumOperation.h"

@implementation SumOperation

- (double) performWithSecondArgument: (double) SecondArgument  {
    return self.firstArgument + SecondArgument;
}

@end
