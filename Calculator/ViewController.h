//
//  ViewController.h
//  Calculator
//
//  Created by Admin on 02.11.16.
//  Copyright © 2016 Admin. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CalculatorModel.h"

@interface ViewController : UIViewController

@property (weak, nonatomic) IBOutlet UILabel *MyLabel;
@property (nonatomic, assign, getter = isNumberInputting) BOOL numberInputting;
@property (nonatomic, strong) CalculatorModel *model;
@property (nonatomic, strong) NSString *lastButton; //check repeating the operations

@end
