//
//  DelOperation.m
//  Calculator
//
//  Created by Admin on 03.11.16.
//  Copyright © 2016 Admin. All rights reserved.
//

#import "DelOperation.h"

@implementation DelOperation

- (double) performWithSecondArgument: (double) SecondArgument  {
    return self.firstArgument / SecondArgument;
}

@end
