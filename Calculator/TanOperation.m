//
//  TanOperation.m
//  Calculator
//
//  Created by Admin on 03.11.16.
//  Copyright © 2016 Admin. All rights reserved.
//

#import "TanOperation.h"

@implementation TanOperation

- (double) perform: (double) value {
    return tan(value);
}

@end
